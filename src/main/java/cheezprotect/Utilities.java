package cheezprotect;

import java.util.Set;

import org.bukkit.entity.ThrownPotion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Sets;

public class Utilities {
    
    private static Set<PotionEffectType> badEffects = Sets.newHashSet(PotionEffectType.HARM, PotionEffectType.POISON, PotionEffectType.CONFUSION);
    
    public static boolean within(int i, int boundMin, int boundMax) {
        return boundMin <= i && i <= boundMax;
    }
    
    public static boolean isBadPotion(ThrownPotion potion) {
        for (PotionEffect e : potion.getEffects())
            if (badEffects.contains(e.getType()))
                return true;
        return false;
    }
    
}
