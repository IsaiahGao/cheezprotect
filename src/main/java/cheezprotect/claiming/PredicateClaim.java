package cheezprotect.claiming;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import cheezprotect.CheezProtect;
import cheezprotect.ProtectionHandler;
import cheezprotect.util.ProtectedRegion2D;
import cheezprotect.util.Region2D;
import cheezprotect.util.SelectionResult;
import cheezprotect.util.WorldChunkWatcher;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.OrdinaryColor;

public class PredicateClaim {

    private static final OrdinaryColor YELLOW = new OrdinaryColor(255, 255, 1);
    private static final OrdinaryColor RED = new OrdinaryColor(255, 1, 1);
    private static final OrdinaryColor ORANGE = new OrdinaryColor(255, 200, 1);
    private static final Random RAND = new Random();
    private Location loc1, loc2;
    private World world;
    private int xmin, xmax, zmin, zmax;
    
    public int ticks;
    
    private boolean cornerSelected;
    private ProtectedRegion2D editing;
    
    private Set<Location> changedBlocks = new HashSet<>();
    
    public static PredicateClaim fromRegion(ProtectedRegion2D region) {
        PredicateClaim predicate = new PredicateClaim();
        predicate.setFirstPoint(region.minPoint());
        predicate.setSecondPoint(region.maxPoint());
        predicate.editing = region;
        predicate.world = region.world;
        return predicate;
    }
    
    public void setEditing(ProtectedRegion2D region) {
        this.editing = region;
    }
    
    public ProtectedRegion2D getEditing() {
        return editing;
    }
    
    public boolean isEditing() {
        return getEditing() != null;
    }
    
    public int area() {
        return (xmax - xmin + 1) * (zmax - zmin + 1);
    }
    
    public int chunkArea() {
        return (int) (Math.ceil((xmax - xmin) / 16) * Math.ceil((zmax - zmin) / 16));
    }
    
    public boolean firstPointSet() {
        return loc1 != null;
    }
    
    public boolean secondPointSet() {
        return loc2 != null;
    }
    
    public Location getFirstPoint() {
        return loc1;
    }
    
    public Location getSecondPoint() {
        return loc2;
    }
    
    public void setFirstPoint(Location loc) {
        loc.setY(loc.getWorld().getHighestBlockYAt(loc));
        loc1 = loc.add(0.5, 1, 0.5);
        world = loc.getWorld();
        ticks = 0;
        check();
    }
    
    public void setSecondPoint(Location loc) {
        loc.setY(loc.getWorld().getHighestBlockYAt(loc));
        loc2 = loc.add(0.5, 1, 0.5);
        world = loc.getWorld();
        ticks = 0;
        check();
    }
    
    public SelectionResult selectCorner(Location loc) {
        ticks = 0;
        
        if (cornerSelected) {
            // no claiming across worlds
            if (loc.getWorld() != world)
                return SelectionResult.FAIL;
            
            setSecondPoint(loc);
            cornerSelected = false;
            return SelectionResult.PLACE_CORNER;
        }
        
        boolean minx, minz;
        if (((minx = loc.getBlockX() == xmin) || loc.getBlockX() == xmax)
                && ((minz = loc.getBlockZ() == zmin) || loc.getBlockZ() == zmax)) {
            loc1.setX((minx ? xmax : xmin) + 0.5);
            loc1.setZ((minz ? zmax : zmin) + 0.5);
            loc2.setX(loc.getBlockX() + 0.5);
            loc2.setZ(loc.getBlockZ() + 0.5);
            world = loc1.getWorld();
            check();
            cornerSelected = true;
            return SelectionResult.SELECT_CORNER;
        }
        return SelectionResult.FAIL;
    }
    
    public ProtectedRegion2D toProtectedRegion(Player owner) {
        return new ProtectedRegion2D(owner.getUniqueId(), loc1, loc2);
    }
    
    private void check() {
        if (loc1 != null && loc2 != null) {
            xmin = Math.min(loc1.getBlockX(), loc2.getBlockX());
            xmax = Math.max(loc1.getBlockX(), loc2.getBlockX());
            zmin = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
            zmax = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
            
            if (area() > ProtectionHandler.getMaxClaim(world))
                loc2 = null;
        }
    }
    
    public boolean checkOverlaps() {
        WorldChunkWatcher watcher = ProtectionHandler.get(world, false);
        
        if (watcher == null)
            return true;
        
        /*if (this.chunkArea() < watcher.totalRegions) {
            for (int x = this.xmin - (this.xmin % 16); x < this.xmax; x += 16) {
                for (int z = this.zmin - (this.zmin % 16); z < this.zmin; z += 16) {
                    PredicateChunk predicate = watcher.getPredicate(world, x / 16, z / 16);
                    if (predicate != null) {
                        for (ProtectedRegion2D region : predicate.getRegions()) {
                            if ((this.editing != null && this.editing != region) && this.intersects(region))
                                return false;
                        }
                    }
                }
            }
        } else {*/
            for (HashSet<ProtectedRegion2D> set : watcher.getAllRegions().values())
                for (ProtectedRegion2D region : set)
                    if (this.editing != region && this.intersects(region))
                        return false;
        //}
        return true;
    }
    
    public boolean intersects(Region2D region) {
        return region.world == this.world && xmin < region.xmax && xmax > region.xmin && zmin < region.zmax && zmax > region.zmin;
    }
    
    public void displayBlocks(final Player p) {
        if (loc1 != null && loc2 != null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Location prev1 = null, prev2 = null;
                    for (double x = xmin + 0.5; x <= xmax + 0.5; x ++) {
                        prev1 = b(world, x, zmin + 0.5, prev1, p);
                        prev2 = b(world, x, zmax + 0.5, prev2, p);
                    }
                    prev1 = prev2 = null;
                    for (double z = zmin + 0.5; z <= zmax + 0.5; z ++) {
                        prev1 = b(world, xmin + 0.5, z, prev1, p);
                        prev2 = b(world, xmax + 0.5, z, prev2, p);
                    }
                }
            }.runTaskLater(CheezProtect.instance, 1L);
        } else {
            if (loc1 != null) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        saveChangedBlock(p, loc1.clone().add(0, -1, 0));
                    }
                }.runTaskLater(CheezProtect.instance, 1L);
            }
            if (loc2 != null) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        saveChangedBlock(p, loc2.clone().add(0, -1, 0));
                    }
                }.runTaskLater(CheezProtect.instance, 1L);
            }
        }
    }
    
    public void display(Player p) {
        if (loc1 != null && loc2 != null) {
            Location prev1 = null, prev2 = null;
            for (double x = xmin + 0.5; x <= xmax + 0.5; x ++) {
                prev1 = a(world, x, zmin + 0.5, prev1, p);
                prev2 = a(world, x, zmax + 0.5, prev2, p);
            }
            prev1 = prev2 = null;
            for (double z = zmin + 0.5; z <= zmax + 0.5; z ++) {
                prev1 = a(world, xmin + 0.5, z, prev1, p);
                prev2 = a(world, xmax + 0.5, z, prev2, p);
            }
        } 
        if (loc1 != null) {
            a(loc1, p, false);
        }
        if (loc2 != null) {
            a(loc2, p, cornerSelected);
        }
    }

    public boolean checkSpawn() {
        if (loc1 == null) {
            return false;
        }
        
        return true;
        //return !this.intersects(ProtectionHandler.spawnRegion);
        /*Location spawn = loc1.getWorld().getSpawnLocation();
        int x1 = spawn.getBlockX();
        int z1 = spawn.getBlockZ();
        int limit = ClaimHandler.SPAWN_DISTANCE * ClaimHandler.SPAWN_DISTANCE;
        
        return ds(x1, z1, xmin, zmin) > limit &&
                ds(x1, z1, xmin, zmax) > limit &&
                ds(x1, z1, xmax, zmax) > limit &&
                ds(x1, z1, xmax, zmin) > limit;*/
    }
    
    private static float ds(int x1, int z1, int x2, int z2) {
        float xd = x2 - x1;
        float zd = z2 - z1;
        return xd * xd + zd * zd;
    }

    private Location b(World world, double x, double z, Location prev, Player p) {
        Location loc = new Location(world, x, 0, z);
        Block b = loc.getWorld().getHighestBlockAt(loc);

        saveChangedBlock(p, b.getLocation());
    
        if (prev != null && prev.getBlockY() != loc.getBlockY()) {
            int ydiff = prev.getBlockY() - loc.getBlockY() < 0 ? -1 : 1; 
            for (int y = loc.getBlockY() + ydiff; y != prev.getBlockY(); y += ydiff) {
                Location clone = loc.clone();
                clone.setY(y);
                if (clone.getBlock().getType().isSolid())
                    saveChangedBlock(p, clone);
            }
        }
        return loc;
    }
    
    @SuppressWarnings("deprecation")
    private void saveChangedBlock(Player p, Location loc) {
        p.sendBlockChange(loc.add(0, -1, 0), Material.GLOWSTONE, (byte) 0);
        changedBlocks.add(loc);
    }
    
    @SuppressWarnings("deprecation")
    public void undisplay(Player p) {
        for (Location loc : changedBlocks) {
            if (loc.getChunk().isLoaded()) {
                Block b = loc.getBlock();
                p.sendBlockChange(loc, b.getType(), b.getData());
            }
        }
        changedBlocks.clear();
    }
    
    private static Location a(World world, double x, double z, Location prev, Player p) {
        Location loc = new Location(world, x, 0, z);
        loc.setY(world.getHighestBlockYAt(loc));
        
        ParticleEffects.RED_DUST.display(YELLOW, loc, p);
    
        if (prev != null && prev.getBlockY() != loc.getBlockY()) {
            int ydiff = prev.getBlockY() - loc.getBlockY() < 0 ? -1 : 1; 
            for (int y = loc.getBlockY() + ydiff; y != prev.getBlockY(); y += ydiff) {
                Location clone = loc.clone();
                clone.setY(y);
                ParticleEffects.RED_DUST.display(YELLOW, clone, p);
            }
        }
        return loc;
    }
    
    public boolean isValid() {
        if (loc1 == null || loc2 == null)
            return false;
        return this.area() > ProtectionHandler.MIN_CLAIM;
    }
    
    private void a(Location loc1, Player p, boolean selected) {
        float xOffset, yOffset, zOffset;
        OrdinaryColor color = selected ? ORANGE : RED;
        for (int i = 0; i < 6; i++) {
            xOffset = RAND.nextFloat() - 0.5F;
            yOffset = RAND.nextFloat() - 0.5F;
            zOffset = RAND.nextFloat() - 0.5F;
            ParticleEffects.RED_DUST.display(color, loc1.clone().add(xOffset * 0.5, yOffset * 0.5, zOffset * 0.5), p);
        }
    }

}
