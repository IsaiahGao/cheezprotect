package cheezprotect.claiming;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import cheezprotect.ProtectionHandler;
import cheezprotect.util.ProtectedRegion2D;
import cwhitelist.Main;

public class ClaimHandler {
    
    public ClaimHandler(Plugin plugin) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!predicates.isEmpty())
                    for (Iterator<Entry<Player, PredicateClaim>> it = predicates.entrySet().iterator(); it.hasNext();) {
                        Entry<Player, PredicateClaim> entry = it.next();
                        //entry.getValue().display(entry.getKey());
                        
                        if (++entry.getValue().ticks > 240) {
                            it.remove();
                            Player p = entry.getKey();
                            p.sendMessage(Main.INFO + "§7Your pending region has been deselected due to inactivity.");
                            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 0.5F);
                            entry.getValue().undisplay(p);
                        }
                    }
            }
        }.runTaskTimer(plugin, 10L, 10L);
    }
    
    private static Map<Player, PredicateClaim> predicates = new WeakHashMap<>();
    
    public static void confirmClaim(Player p) {
        PredicateClaim claim = predicates.get(p);
        if (claim == null) {
            p.sendMessage(Main.ERR + "§cNo pending region found! §7Right Click with a Feather to start one!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
            return;
        }
        
        if (!claim.checkSpawn()) {
            p.sendMessage(Main.ERR + "§cYour claim is too close to Spawn!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
            return;
        }
        
        int area = claim.area();
        if (area < ProtectionHandler.MIN_CLAIM) {
            p.sendMessage(Main.ERR + "§cClaims must be a minimum of " + ProtectionHandler.MIN_CLAIM + " blocks in area! §fCurrent area: §e" + area);
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
            return;
        }
        
        ProtectedRegion2D edit = claim.getEditing();
        int editarea = edit == null ? 0 : edit.area();
        
        int claimed = ProtectionHandler.getClaimedArea(p, p.getWorld());
        int newTotal = claimed - editarea + area;
        if (newTotal > ProtectionHandler.getMaxClaim(p.getWorld())) {
            p.sendMessage(Main.ERR + "§cYou can't claim that much land!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
            return;
        }
        
        if (!claim.checkOverlaps()) {
            p.sendMessage(Main.ERR + "§cYour region selection overlaps another claim!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
            return;
        }
        
        if (edit != null) {
            edit.updateArea(claim);
            p.sendMessage(Main.INFO + "§7Successfully updated §e" + edit.getName() + "'s §7area!");
        } else {
            ProtectedRegion2D region = claim.toProtectedRegion(p);
            p.sendMessage(Main.INFO + "§eSuccessfully claimed some land! §7Use §e/claim settings " + region.getId() + " §7for region options.");
        }
        claim.undisplay(p);
        
        predicates.remove(p);
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.6F);
    }
    
    public static void cancelClaim(Player p) {
        PredicateClaim claim = predicates.remove(p);
        if (claim != null) {
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 0.6F);
            p.sendMessage(Main.INFO + "§7Cancelled region claim.");
            claim.undisplay(p);
        } else {
            p.sendMessage(Main.ERR + "§cNo pending region found! §7Right Click with a Feather to start one!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
        }
    }
    
    public static void claim(Player p, Block b) {
        ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
        PredicateClaim claim = predicates.get(p);
        if (region != null) {
            if (!region.getOwnerUUID().equals(p.getUniqueId())) {
                p.sendMessage(Main.ERR + "§cThis location is already claimed.");
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
                return;
            }
            
            if (claim == null) {
                predicates.put(p, claim = PredicateClaim.fromRegion(region));
                p.sendMessage(Main.INFO + "§eSelected region. §7Right Click a corner to edit.");
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 1.6F);
                claim.displayBlocks(p);
            } else {
                switch (claim.selectCorner(b.getLocation())) {
                    case FAIL:
                        p.sendMessage(Main.ERR + "§cRight Click a corner to select for repositioning.");
                        p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
                        break;
                    case PLACE_CORNER:
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 2.0F);
                        break;
                    case SELECT_CORNER:
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 0.5F);
                        break;
                }
                claim.undisplay(p);
                claim.displayBlocks(p);
            }
        } else {
            if (claim == null) {
                predicates.put(p, claim = new PredicateClaim());
            }
            
            if (claim.secondPointSet()) {
                switch (claim.selectCorner(b.getLocation())) {
                case FAIL:
                    p.sendMessage(Main.ERR + "§cRight Click a corner to select for repositioning.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
                    break;
                case PLACE_CORNER:
                    p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 2.0F);
                    break;
                case SELECT_CORNER:
                    p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 0.5F);
                    break;
                }
                claim.undisplay(p);
                claim.displayBlocks(p);
            } else if (claim.firstPointSet()) {
                claim.setSecondPoint(b.getLocation());
                p.playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 1.0F, 1.2F);
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 1.6F);
                p.sendMessage(Main.INFO + "§eSelection completed! §7Right Click a corner to edit.");
                p.sendMessage(Main.INFO + "§7Use §e/claim confirm §7to claim, or §e/claim cancel§7 to clear.");
                claim.undisplay(p);
                claim.displayBlocks(p);
            } else {
                claim.setFirstPoint(b.getLocation());
                p.playSound(p.getLocation(), Sound.BLOCK_LEVER_CLICK, 1.0F, 0.8F);
                p.sendMessage(Main.INFO + "§eStarted selection! §7Right Click to select the other corner.");
                claim.displayBlocks(p);
            }
        }
    }

}
