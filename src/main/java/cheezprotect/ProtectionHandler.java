package cheezprotect;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import cheezprotect.util.PredicateChunk;
import cheezprotect.util.ProtectedRegion2D;
import cheezprotect.util.Region2D;
import cheezprotect.util.WorldChunkWatcher;
import cwhitelist.Main;

public class ProtectionHandler {
    
    public static Map<World, WorldChunkWatcher> watchedWorlds = new HashMap<>();
    public static Map<String, ProtectedRegion2D> regionCache = new HashMap<>();
    
    public static Map<UUID, Map<String, Integer>> claimAreas = new HashMap<>();
    public static Map<String, Integer> maxClaim = new HashMap<>();

    //public static Region2D spawnRegion;
    
    public static final int MIN_CLAIM = 49;

    static {
        maxClaim.put("world", 22500);
        maxClaim.put("creative", 40000);
        
        //World world = Bukkit.getWorld("world");
        //Location spawn = world.getSpawnLocation();
        //spawnRegion = new Region2D(spawn.clone().add(-24, 0, -24), spawn.clone().add(24, 0, 24));
    }
    
    public static int getMaxClaim(World w) {
        return maxClaim.getOrDefault(w.getName(), 0);
    }
    
    public static WorldChunkWatcher registerWorld(World w) {
        WorldChunkWatcher watcher = new WorldChunkWatcher(w);
        watchedWorlds.put(w, watcher);
        return watcher;
    }
    
    public static Collection<ProtectedRegion2D> getRegions(UUID id) {
        if (watchedWorlds.size() == 1) {
            for (WorldChunkWatcher w : watchedWorlds.values())
                return w.getClaimedRegions(id);
        }
        
        List<ProtectedRegion2D> list = new LinkedList<>();
        for (WorldChunkWatcher w : watchedWorlds.values())
            list.addAll(w.getClaimedRegions(id));
        return list;
    }
    
    public static int getClaimedArea(Player p, World world) {
        Map<String, Integer> map = claimAreas.get(p.getUniqueId());
        if (map == null)
            return 0;
        return map.getOrDefault(world.getName(), 0);
    }
    
    public static void addClaimedArea(UUID owner, World world, int amount) {
        if (world == null)
            return;
        
        Map<String, Integer> map = claimAreas.get(owner);
        if (map == null)
            claimAreas.put(owner, map = new HashMap<String, Integer>());
        map.put(world.getName(), map.getOrDefault(world.getName(), 0) + amount);
    }
    
    public static void addToRegionCache(ProtectedRegion2D region) {
        regionCache.put(region.getId(), region);
    }
    
    public static void registerProtection(ProtectedRegion2D region) {
        get(region.world, true).registerProtection(region);
    }
    
    public static boolean deleteRegion(final Player p, String[] args, boolean force) {
        final ProtectedRegion2D region = ProtectionHandler.getRegion(p, args, 1);
        if (region == null) {
            p.sendMessage(Main.ERR + (args.length > 1 ? "§cNo such region with ID: §e" + args[1] : "§cYou aren't standing in any claimed regions!"));
            return false;
        }
        
        if (!force && !region.getOwnerUUID().equals(p.getUniqueId())) {
            p.sendMessage(Main.ERR + "§cYou can only unclaim your own regions.");
            return false;
        }
        
        if (!force && (!p.hasMetadata("drc") || region.getId() != p.getMetadata("drc").get(0).asString())) {
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 0.6F);
            p.sendMessage(Main.INFO + "§6Are you sure you want to delete the region: §e" + region.getName() + "§6?");
            p.sendMessage(Main.INFO + "§6Use the command again to confirm deletion.");
            p.setMetadata("drc", new FixedMetadataValue(Main.instance(), region.getId()));
           
            int task = new BukkitRunnable() {
                @Override
                public void run() {
                    p.removeMetadata("drc", Main.instance());
                    p.sendMessage(Main.INFO + "§e" + region.getName() + "§f will not be deleted.");
                }
            }.runTaskLater(Main.instance(), 100L).getTaskId();
            p.setMetadata("deltask", new FixedMetadataValue(CheezProtect.instance, task));
            return true;
        }
        
        deleteRegion(region);
        p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 0.9F);
        p.sendMessage(Main.INFO + "§cDeleted claim with ID: §f" + region.getId());
        
        if (p.hasMetadata("deltask"))
            Bukkit.getScheduler().cancelTask(p.getMetadata("deltask").get(0).asInt());
        
        return true;
    }
    
    public static void deleteRegion(ProtectedRegion2D region) {
        regionCache.remove(region.getId());
        watchedWorlds.get(region.world).removeProtection(region);
        addClaimedArea(region.getOwnerUUID(), region.getWorld(), -region.area());
    }
    
    public static WorldChunkWatcher get(World w, boolean registerIfNonexistent) {
        WorldChunkWatcher watcher = watchedWorlds.get(w);

        if (watcher == null && registerIfNonexistent) {
            watchedWorlds.put(w, watcher = new WorldChunkWatcher(w));
        }
        return watcher;
    }
    
    public static boolean isClaimed(Location loc) {
        return getRegion(loc) != null;
    }
    
    // O(1) if doesnt exist
    // O(m) if exists; m = #claimed regions in chunk, expected small
    public static ProtectedRegion2D getRegion(Location loc) {
        WorldChunkWatcher w = watchedWorlds.get(loc.getWorld());
        if (w == null) {
            return null;
        }

        PredicateChunk pc = w.getPredicate(loc.getChunk());
        if (pc == null) {
            return null;
        }

        return pc.get(loc);
    }
    
    public static ProtectedRegion2D getRegion(String id) {
        return regionCache.get(id);
    }
    
    public static ProtectedRegion2D getRegion(Player p, String[] args, int index) {
        ProtectedRegion2D region = null;
        if (args.length > index) {
            region = ProtectionHandler.getRegion(args[index]);
        } else {
            region = ProtectionHandler.getRegion(p.getLocation());
        }
        return region;
    }
    
    public static void addOrRemove(boolean add, Player p, String[] args, boolean force) {
        if (args.length < 2) {
            p.sendMessage(Main.ERR + "§cInvalid command syntax. §f/claim add [Player] [ID?]");
            return;
        }
        
        OfflinePlayer o = Bukkit.getOfflinePlayer(args[1]);
        if (o == null || !o.hasPlayedBefore()) {
            o = Main.instance().getPlayer(args[1]);
            
            if (o == null) {
                p.sendMessage(Main.ERR + "§cInvalid player specified. §f/claim add [Player] [ID?]");
                return;
            }
        }
        
        if (o.getUniqueId().equals(p.getUniqueId())) {
            p.sendMessage(Main.ERR + "§cYou can't add or remove yourself from your own claim.");
            return;
        }
        
        ProtectedRegion2D region = getRegion(p, args, 2);
        if (region == null) {
            p.sendMessage(Main.ERR + (args.length > 2 ? "§cNo such region with ID: §e" + args[2] : "§cYou aren't standing in any claimed regions!"));
            return;
        }
        
        if (!force && !region.getOwnerUUID().equals(p.getUniqueId())) {
            p.sendMessage(Main.ERR + "§cYou do not own the region with ID: §e" + args[1]);
            return;
        }
        
        if (add)
            region.addMember(o);
        else
            region.removeMember(o);
    }
    
    public static boolean canInteract(Player p, Location loc) {
        WorldChunkWatcher watcher = watchedWorlds.get(loc.getWorld());
        if (watcher != null) {
            return watcher.canInteract(p, loc);
        }
        return false;
    }
    
    public static void load(FileConfiguration config) {
        for (String s : config.getConfigurationSection("regions").getKeys(false)) {
            new ProtectedRegion2D(config.getConfigurationSection("regions").getConfigurationSection(s));
        }
        
        /*for (String s : config.getConfigurationSection("data").getKeys(false)) {
            new WorldChunkWatcher(config.getConfigurationSection("data." + s));
        }*/
    }
    
    public static void save(FileConfiguration config) {
        //config.set("data", new HashMap<Object, Object>());
        config.set("regions", new HashMap<Object, Object>());
        
        ConfigurationSection regions = config.getConfigurationSection("regions");
        if (regions == null)
            regions = config.createSection("regions");
        
        for (Entry<String, ProtectedRegion2D> entry : regionCache.entrySet()) {
            if (entry.getValue() == null) {
                continue;
            }
            ConfigurationSection section = regions.createSection(entry.getKey());
            entry.getValue().save(section);
        }
        
        /*ConfigurationSection data = config.getConfigurationSection("data");
        if (data == null)
            data = config.createSection("data");
        
        for (WorldChunkWatcher watcher : watchedWorlds.values()) {
            watcher.save(data.createSection(watcher.getWorld().getName()));
        }*/
    }

}
