package cheezprotect.listeners;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.EquipmentSlot;

import cheezprotect.ProtectionHandler;
import cheezprotect.claiming.ClaimHandler;
import cheezprotect.util.ProtectedRegion2D;
import cwhitelist.Main;
import utilities.Utils;
import utilities.gui.ActionBar;

public class RegionListener implements Listener {
    
    public static boolean inOverworld(World world) {
        return world == Main.overworld || world == Main.creative;
    }
    
    @EventHandler
    public void playerMove(PlayerMoveEvent e) {
        if (!inOverworld(e.getTo().getWorld())) {
            return;
        }
        
        if (e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()) {
            ProtectedRegion2D to = ProtectionHandler.getRegion(e.getTo());
            ProtectedRegion2D from = ProtectionHandler.getRegion(e.getFrom());
            if (to != from) {
                Player p = e.getPlayer();
                new ActionBar().sendMessage(p, to == null ? 
                                "[ Leaving §c" + from.getName() + "§f! ]"
                                : "[ Entering §a" + to.getName() + "§f! ]");
            }
        }
    }
    
    @EventHandler
    public void playerTeleport(PlayerTeleportEvent e) {
        ProtectedRegion2D to = ProtectionHandler.getRegion(e.getTo());
        ProtectedRegion2D from = ProtectionHandler.getRegion(e.getFrom());
        if (to != from) {
            Player p = e.getPlayer();
            new ActionBar().sendMessage(p, to == null ? 
                            "[ Leaving §c" + from.getName() + "§f! ]"
                            : "[ Entering §a" + to.getName() + "§f! ]");
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void interact(PlayerInteractEvent e) {
        if (!inOverworld(e.getPlayer().getWorld()))
            return;
        
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getHand() == EquipmentSlot.HAND && e.getItem() != null && e.getItem().getType() == Material.FEATHER && !Utils.hasGlow(e.getItem())) {
                ClaimHandler.claim(p, e.getClickedBlock());
            }
        }
    }

}
