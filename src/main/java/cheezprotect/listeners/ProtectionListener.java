package cheezprotect.listeners;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.TravelAgent;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.hanging.HangingBreakEvent.RemoveCause;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.LightningStrikeEvent.Cause;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.projectiles.BlockProjectileSource;

import cheezprotect.ProtectionHandler;
import cheezprotect.Utilities;
import cheezprotect.util.ProtectedRegion2D;
import cwhitelist.Main;
import cwhitelist.loottables.LootTracker;
import me.crafter.mc.lockettepro.LocketteProAPI;
import utilities.particles.ParticleEffects;

public class ProtectionListener implements Listener {
	
	public static boolean isProtected(Entity victim) {
		return victim instanceof ArmorStand || victim instanceof Animals || victim instanceof Villager
			|| victim instanceof ItemFrame || victim instanceof Painting || victim instanceof Fish
			|| victim instanceof Dolphin;
	}
    
    @EventHandler
    public void zombieBreakDoor(EntityBreakDoorEvent e) {
        Block b = e.getBlock();
        if (ProtectionHandler.isClaimed(b.getLocation())) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void playerInteract(PlayerInteractEvent e) {
        if (e.getClickedBlock() != null) {
            Block b = e.getClickedBlock();
        	if (e.getAction() == Action.RIGHT_CLICK_BLOCK &&e.getItem() != null &&  e.getItem().getType() == Material.SNOW_BLOCK) {
                ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
                if (region != null && !region.can(e.getPlayer())) {
	                e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from placing that.");
	                e.setCancelled(true);
	                return;
                }
        	}
        	
            if (e.getItem() != null && e.getItem().getType() == Material.SIGN) {
                ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
                if (region != null && !region.can(e.getPlayer())) {
                    e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from placing that.");
                    e.setCancelled(true);
                    return;
                }
            }
                        
            switch (b.getType()) {
                default:
                    if (!(b.getState() instanceof ShulkerBox))
                        break;
                case CHEST:
                case TRAPPED_CHEST:
                    if (LootTracker.isLootChest(b))
                        return;
                    
                case FURNACE:
                case BREWING_STAND:
                case HOPPER:
                case DROPPER:
                case DISPENSER:
                case FLOWER_POT:
                    Player p = e.getPlayer();
                    
                    if (LocketteProAPI.isUser(b, p))
                        return;
                    
                    ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
                    if (region != null && !region.can(p)) {
                        e.setCancelled(true);
                        if (e.getHand() == EquipmentSlot.HAND)
                            p.sendMessage(Main.ERR + "§cA magical force prevents you from opening that.");
                        ParticleEffects.SMOKE.display(0.2F, 0.2F, 0.2F, 0.03F, 8, b.getLocation().add(0.5, 1.2, 0.5), p);
                        p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.6F, 1.2F);
                    }
                    break;
                case FARMLAND:
                    if (e.getAction() == Action.PHYSICAL) {
                        region = ProtectionHandler.getRegion(e.getClickedBlock().getLocation());
                        if (region != null) {
                            e.setCancelled(true);
                        }
                    }
                    break;
                case FIRE:
                    if (e.getAction() == Action.LEFT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.FIRE) {
                        p = e.getPlayer();

                        region = ProtectionHandler.getRegion(b.getLocation());
                        if (region != null && !region.can(p)) {
                            e.setCancelled(true);
                            p.sendMessage(Main.ERR + "§cA magical force prevents you from extinguishing that.");
                        }
                    }
                    break;
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void bucketFill(PlayerBucketFillEvent e) {
        if (e.getBlockClicked() != null) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(e.getBlockClicked().getLocation());
            if (region != null && !region.can(e.getPlayer())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from doing that.");
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void bucketEmpty(PlayerBucketEmptyEvent e) {
        if (e.getBlockClicked() != null) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(e.getBlockClicked().getLocation());
            if (region != null && !region.can(e.getPlayer())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from pouring that.");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void entityTrample(EntityInteractEvent e) {
        if (e.getBlock() != null && e.getBlock().getType() == Material.FARMLAND) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(e.getBlock().getLocation());
            if (region != null) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void entityInteract(PlayerInteractEntityEvent e) {
        Entity entity = e.getRightClicked();
        switch (entity.getType()) {
            case MINECART_CHEST:
                if (LootTracker.isLootChest(entity))
                    return;
                
            case MINECART_FURNACE:
            case MINECART_HOPPER:
                ProtectedRegion2D region = ProtectionHandler.getRegion(entity.getLocation());
                Player p = e.getPlayer();
                if (region != null && !region.can(p)) {
                    e.setCancelled(true);
                    if (e.getHand() == EquipmentSlot.HAND)
                        p.sendMessage(Main.ERR + "§cA magical force prevents you from opening that.");
                }
                break;
            case ITEM_FRAME:
                region = ProtectionHandler.getRegion(entity.getLocation());
                p = e.getPlayer();
                if (region != null && !region.can(p)) {
                    e.setCancelled(true);
                    p.sendMessage(Main.ERR + "§cA magical force prevents you from rotating that.");
                    p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.6F, 1.2F);
                }
                break;
            case COD:
            case SALMON:
            case PUFFERFISH:
            case TROPICAL_FISH:
            case DOLPHIN:
                region = ProtectionHandler.getRegion(entity.getLocation());
                p = e.getPlayer();
                if (region != null && !region.can(p)) {
                    e.setCancelled(true);
                    p.sendMessage(Main.ERR + "§cA magical force prevents you from messing with that fish.");
                    p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.6F, 1.2F);
                }
                break;
            default:
                break;
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void blockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        Block b = e.getBlockPlaced();
        ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
        if (region != null && !region.can(p)) {
            e.setCancelled(true);
            p.sendMessage(Main.ERR + "§cA magical force prevents you from placing that.");
            ParticleEffects.SMOKE.display(0.2F, 0.2F, 0.2F, 0.03F, 8, b.getLocation().add(0.5, 0.2, 0.5), p);
            p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.6F, 1.2F);
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void blockPlace(BlockBreakEvent e) {
        Player p = e.getPlayer();
        Block b = e.getBlock();
        ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
        if (region != null && !region.can(p)) {
            e.setCancelled(true);
            p.sendMessage(Main.ERR + "§cA magical force prevents you from breaking that.");
            ParticleEffects.SMOKE.display(0.2F, 0.2F, 0.2F, 0.03F, 8, b.getLocation().add(0.5, 0.2, 0.5), p);
            p.playSound(p.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.6F, 1.2F);
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void eggHatch(PlayerEggThrowEvent e) {
        if (!e.isHatching())
            return;
        
        Location loc = e.getEgg().getLocation();
        ProtectedRegion2D region = ProtectionHandler.getRegion(loc);
        if (region != null && !region.can(e.getPlayer())) {
            e.setHatching(false);
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void potionSplash(PotionSplashEvent e) {
        if (!Utilities.isBadPotion(e.getPotion()))
            return;
        
        Projectile potion = e.getPotion();
        
        boolean playerShooter = false;
        if (!(playerShooter = potion.getShooter() instanceof Player) && !(potion.getShooter() instanceof BlockProjectileSource))
            return;
        
        Location loc = potion.getLocation();
        ProtectedRegion2D region = ProtectionHandler.getRegion(loc);
        if (region != null && (playerShooter ? !region.can((Player) potion.getShooter()) :
            region != ProtectionHandler.getRegion(((BlockProjectileSource) potion.getShooter()).getBlock().getLocation()))) {
            try {
                Field f = e.getClass().getDeclaredField("affectedEntities");
                f.setAccessible(true);
                ((Map<?, ?>) f.get(e)).clear();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return;
        }
        
        for (LivingEntity entity : e.getAffectedEntities()) {
            ProtectedRegion2D region2 = ProtectionHandler.getRegion(entity.getLocation());
            if (region2 != null) {
                if (entity instanceof Player) {
                    if (!region2.isPVP())
                        e.setIntensity(entity, 0);
                } else if (isProtected(entity)) {
                    if ((playerShooter ? !region2.can((Player) potion.getShooter()) :
                        region2 != ProtectionHandler.getRegion(((BlockProjectileSource) potion.getShooter()).getBlock().getLocation())))
                        e.setIntensity(entity, 0);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void entityExplode(EntityExplodeEvent e) {
        if (!RegionListener.inOverworld(e.getEntity().getWorld()))
            return;
        
        Entity tntSource = null;
        switch (e.getEntityType()) {
            case PRIMED_TNT:
                tntSource = ((TNTPrimed) e.getEntity()).getSource();
                if (!(tntSource instanceof Player)) {
                    tntSource = null;
                }
            case CREEPER:
            case FIREBALL:
                for (Iterator<Block> it = e.blockList().iterator(); it.hasNext();) {
                    Block b = it.next();
                    ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
                    if (region != null) {
                        if (tntSource == null || !region.can((Player) tntSource))
                            it.remove();
                    }
                }
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void fireSpread(BlockSpreadEvent e) {
        if (!RegionListener.inOverworld(e.getBlock().getWorld()))
            return;
        
        if (e.getSource().getType() != Material.FIRE)
            return;
        
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getNewState().getLocation());
        if (region != null)
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void fireburn(BlockBurnEvent e) {
        if (!RegionListener.inOverworld(e.getBlock().getWorld()))
            return;
        
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getBlock().getLocation());
        if (region != null)
            e.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void pistonpull(BlockPistonExtendEvent e) {
        if (!RegionListener.inOverworld(e.getBlock().getWorld()))
            return;
        
        ProtectedRegion2D blockRegion = ProtectionHandler.getRegion(e.getBlock().getLocation());
        
        for (Block b : e.getBlocks()) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(b.getRelative(e.getDirection()).getLocation());
            if (region != null && region != blockRegion) {
                e.setCancelled(true);
                break;
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void pistonpull(BlockPistonRetractEvent e) {
        if (!RegionListener.inOverworld(e.getBlock().getWorld()))
            return;
        
        if (!e.isSticky())
            return;
        
        ProtectedRegion2D blockRegion = ProtectionHandler.getRegion(e.getBlock().getLocation());
        
        for (Block b : e.getBlocks()) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(b.getLocation());
            if (region != null && region != blockRegion) {
                e.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void ignite(BlockIgniteEvent e) {
        if (!RegionListener.inOverworld(e.getBlock().getWorld()))
            return;

        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getBlock().getLocation());
        if (region != null) {
            if (e.getIgnitingEntity() instanceof Player) {
                if (!region.can((Player) e.getIgnitingEntity())) {
                    e.setCancelled(true);
                    e.getIgnitingEntity().sendMessage(Main.ERR + "§cA magical force prevents you from arsoning this place.");
                }
            } else {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void entityignite(EntityCombustByEntityEvent e) {
        if (!RegionListener.inOverworld(e.getEntity().getWorld()))
            return;
        
        if ((e.getEntity() instanceof Player || e.getEntity() instanceof Tameable || isProtected(e.getEntity())) && e.getCombuster() instanceof Arrow) {
            Entity victim = e.getEntity();
            
            ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
            if (region != null && !region.isPVP()) {
                e.setDuration(0);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void entitydmg(EntityDamageByEntityEvent e) {
        if (!RegionListener.inOverworld(e.getEntity().getWorld()))
            return;

        Entity damager = e.getDamager();
        Entity victim = e.getEntity();
        
        if (damager instanceof Projectile) {
            if (((Projectile) damager).getShooter() instanceof Entity)
                damager = (Entity) ((Projectile) damager).getShooter();
        } else if (damager instanceof TNTPrimed) {
            ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
            if (region != null) {
                TNTPrimed tnt = (TNTPrimed) damager;
                if (isProtected(victim)) {
                    if (tnt.getSource() == null || (tnt.getSource() instanceof Player && !region.can((Player) tnt.getSource()))) {
                        e.setCancelled(true);
                        return;
                    }
                } else if (victim instanceof Player) {
                    if (!region.isPVP() && !tnt.hasMetadata("fished")) {
                        e.setCancelled(true);
                        return;
                    }
                }
            }
            return;
        } else if (e.getDamager() instanceof LightningStrike) {
            LightningStrike ls = (LightningStrike) e.getDamager();
            if (!ls.hasMetadata("lightning"))
                return;
            
            ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
            damager = Bukkit.getPlayer(ls.getMetadata("lightning").get(0).asString());
            if (damager == null)
                return;
                
            if (region != null) {
                if (isProtected(victim)) {
                    e.setCancelled(true);
                } else if (victim instanceof Player) {
                    if (!region.isPVP()) {
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }
        
        if (isProtected(victim)) {
            if (damager instanceof Player) {
                ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
                
                if (region == null)
                    return;
                
                if (victim instanceof Tameable) {
                    Tameable tameable = (Tameable) victim;
                    if (tameable.getOwner() != damager && !region.isPVP()) {
                        if (!((Player) damager).isSneaking())
                            damager.sendMessage(Main.ERR + "§cA magical force prevents you from hitting that here.");
                        e.setCancelled(true);
                        return;
                    }
                } else if (!region.can((Player) damager)) {
                    damager.sendMessage(Main.ERR + "§cA magical force prevents you from hitting that here.");
                    e.setCancelled(true);
                    return;
                }
            } else if (damager instanceof Creeper || damager instanceof Skeleton || damager instanceof Guardian) {
                ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
                if (region != null) {
                    e.setCancelled(true);
                }
            }
        } else if (victim instanceof Player && (damager instanceof Player || (damager instanceof Wolf && ((Wolf) damager).isTamed()))) {
            if (e.getDamager() instanceof Player && ((Player) e.getDamager()).isSneaking()) {
                return;
            }
            
            ProtectedRegion2D region = ProtectionHandler.getRegion(victim.getLocation());
            if (region != null && !region.isPVP() && damager != victim) {
                e.setCancelled(true);
                damager.sendMessage(Main.ERR + "§cPVP is disabled in this claim.");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void hangingbreak(HangingBreakByEntityEvent e) {
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getEntity().getLocation());
        if (region != null) {
            if (e.getRemover() instanceof Player && region.can((Player) e.getRemover()))
                return;
            e.setCancelled(true);
            e.getRemover().sendMessage(Main.ERR + "§cA magical force prevents you from breaking that here.");
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void hangingplace(HangingPlaceEvent e) {
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getEntity().getLocation());
        if (region != null) {
            if (region.can(e.getPlayer()))
                return;
            e.setCancelled(true);
            e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from placing that here.");
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void hangingbreak(HangingBreakEvent e) {
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getEntity().getLocation());
        if (region != null) {
            if (e.getCause() == RemoveCause.EXPLOSION)
                e.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void armorStands(PlayerArmorStandManipulateEvent e) {
        ProtectedRegion2D region = ProtectionHandler.getRegion(e.getRightClicked().getLocation());
        if (region != null) {
            if (region.can(e.getPlayer()))
                return;
            e.setCancelled(true);
            e.getPlayer().sendMessage(Main.ERR + "§cA magical force prevents you from messing with that here.");
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void portalCreate(PlayerPortalEvent e) {
        if (e.getTo() != null && e.getTo().getWorld() == Main.overworld) {
            if (ProtectionHandler.getRegion(e.getTo()) != null) {
                TravelAgent ta = e.getPortalTravelAgent();
                ta.setCanCreatePortal(false);
            }
        }
    }
    
    @EventHandler
    public void lightning(LightningStrikeEvent e) {
        LightningStrike entity = e.getLightning();
        if (entity.isEffect())
            return;
        
        for (Entity near : entity.getNearbyEntities(6.5, 12.5, 6.5)) {
            if (near.getType() == EntityType.VILLAGER || near.getType() == EntityType.PIG) {
                ProtectedRegion2D region = ProtectionHandler.getRegion(near.getLocation());
                
                if (region != null) {
                    e.setCancelled(true);
                    e.getWorld().strikeLightningEffect(entity.getLocation());
                    return;
                }
            }
        }
        
        if (e.getCause() == Cause.TRIDENT) {
        	e.getLightning().setMetadata("lightning", new FixedMetadataValue(Main.instance(), true));
        }
    }
}
