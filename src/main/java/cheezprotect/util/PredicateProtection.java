package cheezprotect.util;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import utilities.Utils;

public class PredicateProtection {
    
    public enum ProtectionParameter {
    	// TODO DARK_OAK_DOOR vs DARK_OAK_DOOR_ITEM ?
        CAN_OPEN_DOORS(0, 0, Utils.ConstructItemStack(Material.DARK_OAK_DOOR, 1, (short) 0, "§eDoor Access", new String[] {"§9Who can open doors?"}, false)),
        CAN_USE_REDSTONE(1, 0, Utils.ConstructItemStack(Material.LEVER, 1, (short) 0, "§eRedstone Access", new String[] {"§9Who can operate buttons, levers, etc?"}, false)),
        CAN_OPEN_CONTAINERS(2, 1, Utils.ConstructItemStack(Material.CHEST, 1, (short) 0, "§eContainer Access", new String[] {"§9Who can open containers (e.g. chests)?"}, false)),
        CAN_HURT_ANIMALS(3, 1, Utils.ConstructItemStack(Material.PORKCHOP, 1, (short) 0, "§eSlaughterhouse Access", new String[] {"§9Who can kill peaceful animals?"}, false)),
        CAN_BREED_ANIMALS(4, 1, Utils.ConstructItemStack(Material.WHEAT, 1, (short) 0, "§eBreeding Access", new String[] {"§9Who can breed animals?"}, false)),
        CAN_RIDE_ANIMALS(5, 1, Utils.ConstructItemStack(Material.CARROT_ON_A_STICK, 1, (short) 0, "§eMount Access", new String[] {"§9Who can ride animals?"}, false)),
        CAN_TRADE_VILLAGERS(6, 1, Utils.ConstructItemStack(Material.EMERALD, 1, (short) 0, "§eVillager Access", new String[] {"§9Who can trade with villagers?"}, false)),
        CAN_BUILD(7, 1, Utils.ConstructEnchantedItemStack(Material.GRASS, 1, (short) 0, "§eBuild Access", new String[] {"§9Who can place and destroy blocks?"}, false)),
        CAN_OPEN_FUNCTIONS(8, 0, Utils.ConstructItemStack(Material.CRAFTING_TABLE, 1, (short) 0, "§eFunctional Access", new String[] {"§9Who can use Crafting Tables, Furnaces, etc?"}, false)),
        CAN_THROW_STUFF(9, 0, Utils.ConstructItemStack(Material.EGG, 1, (short) 0, "§eThrowable Access", new String[] {"§9Who can throw stuff like eggs and splash potions?"}, false));
        
        private ProtectionParameter(int index, int def, ItemStack icon) {
            this.index = index;
            this.def = def;
        }
        
        private int index;
        private int def;
    }
    
    public PredicateProtection(boolean init) {
        this.data = new int[ProtectionParameter.values().length];
        
        if (init) {
            for (ProtectionParameter p : ProtectionParameter.values()) {
                this.data[p.index] = p.def;
            }
        }
    }
    
    public PredicateProtection(ConfigurationSection config) {
        this(false);
        
        for (ProtectionParameter p : ProtectionParameter.values()) {
            try {
                this.data[p.index] = config.getInt(a(p.toString()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private int[] data;
    
    public int get(ProtectionParameter parameter) {
        return data[parameter.index];
    }
    
    public boolean can(ProtectionParameter parameter, int role) {
        return role >= get(parameter);
    }
    
    public void set(ProtectionParameter parameter, int newRole) {
        data[parameter.index] = newRole;
    }
    
    public void save(ConfigurationSection config) {
        for (ProtectionParameter p : ProtectionParameter.values()) {
            try {
                config.set(a(p.toString()), this.data[p.index]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private static String a(String s) {
        return s.replace('_', '-');
    }

}
