package cheezprotect.util;

public enum SelectionResult {
    
    FAIL,
    SELECT_CORNER,
    PLACE_CORNER

}
