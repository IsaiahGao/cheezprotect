package cheezprotect.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import cheezprotect.CheezProtect;
import cheezprotect.ProtectionHandler;
import cheezprotect.claiming.PredicateClaim;
import cheezprotect.util.PredicateProtection.ProtectionParameter;
import cwhitelist.Main;
import cwhitelist.anvilgui.AnvilGUI;
import cwhitelist.anvilgui.AnvilGUI.AnvilSlot;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_13_R2.AxisAlignedBB;
import utilities.Utils;

public class ProtectedRegion2D extends Region2D {
    
    public static final ItemStack NAME = Utils.ConstructItemStack(Material.SIGN, 1, (short) 0, "§eTerritory Name", new String[] {"", "§9[Click to Change]"}, false);
    public static final ItemStack PVP = Utils.ConstructItemStack(Material.IRON_SWORD, 1, (short) 0, "§ePVP", new String[] {"", "§9[Click to Toggle]"}, false);
    public static final ItemStack MEMBERS = Utils.ConstructItemStack(Material.PLAYER_HEAD, 1, (short) 3, "§eMember List", new String[] {"§9[Click to View]"}, false);
    public static final ItemStack PROTECT = Utils.ConstructItemStack(Material.IRON_DOOR, 1, (short) 0, "§eProtection Status", new String[] {"§7Anyone can build on unprotected land.", "", "§9[Click to Toggle]"}, false);
    
    private static int i = 0;
    private static int c = 0;

    public ProtectedRegion2D(UUID owner, Location loc1, Location loc2) {
        super(loc1, loc2);
        this.owner = owner;
        this.mods = new LinkedList<>();
        this.members = new LinkedList<>();
        this.roles = new HashMap<>();
        this.containers = new HashSet<>();
        this.predicate = new PredicateProtection(true);
        this.isProtected = true;
        this.name = Main.getName(owner) + "'s land";
        this.sid = loc1.getWorld() == Main.creative ? "c" + c++ : "w" + i++;
        
        this.initialized = true;
        
        markChunks();

        ProtectionHandler.addToRegionCache(this);
        ProtectionHandler.registerProtection(this);
        ProtectionHandler.addClaimedArea(owner, world, area());
    }
    
    public ProtectedRegion2D(ConfigurationSection config) {
        super(config);
        if (world != Main.creative && new File("reset").exists()) {
            // resetting, so don't load any regions
            return;
        }
        
        
        this.mods = new ArrayList<>();
        this.members = new ArrayList<>();
        this.roles = new HashMap<>();
        this.containers = new HashSet<>();
        this.sid = config.getName();
        int id = Utils.parseInt(sid.substring(1), Utils.random(Integer.MAX_VALUE));
        if (this.world == Main.creative) {
            if (id >= c)
                c = id + 1;
        } else if (id >= i)
            i = id + 1;
        
        this.isProtected = config.contains("protected") ? config.getBoolean("protected") : true;
        this.isPvp = config.contains("pvp") ? config.getBoolean("pvp") : false;
        this.owner = UUID.fromString(config.getString("owner"));
        this.name = config.getString("name");
        for (String player : config.getStringList("members")) {
            String[] arr = player.split(",");
            UUID uid = UUID.fromString(arr[1]);
            int role = Integer.parseInt(arr[2]);
            
            roles.put(uid, role);
            if (role >= 2)
                mods.add(uid);
            else
                members.add(uid);
        }
        this.predicate = new PredicateProtection(config.getConfigurationSection("predicate"));
        markChunks();
        
        ProtectionHandler.addToRegionCache(this);
        // why not this here?
        ProtectionHandler.registerProtection(this);
        ProtectionHandler.addClaimedArea(owner, world, area());
    }
    
    private String sid;
    private UUID owner;
    private String name;
    private List<UUID> mods, members;
    private Map<UUID, Integer> roles;
    private Set<PredicateChunk> containers;
    private PredicateProtection predicate;
    
    private boolean isProtected = true, isPvp = false;
    public boolean initialized = false;
    
    public String getId() {
        return sid;
    }
    
    public UUID getOwnerUUID() {
        return owner;
    }
    
    public OfflinePlayer getOwner() {
        return Bukkit.getOfflinePlayer(owner);
    }
    
    public void addContainer(PredicateChunk predicate) {
        containers.add(predicate);
    }
    
    public Set<PredicateChunk> getContainers() {
        return containers;
    }
    
    public PredicateProtection getProtection() {
        return predicate;
    }
    
    public boolean isPVP() {
        return isPvp;
    }
    
    public boolean togglePVP() {
        return isPvp = !isPvp;
    }
    
    public boolean isMember(OfflinePlayer p) {
        return p.getUniqueId().equals(getOwnerUUID()) || roles.containsKey(p.getUniqueId());
    }
    
    public Iterable<Player> getPlayersInRegion(Entity except) {
        List<Player> players = new LinkedList<>();
        net.minecraft.server.v1_13_R2.World nmsWorld = ((CraftWorld) world).getHandle();
        for (net.minecraft.server.v1_13_R2.Entity nmsEntity : nmsWorld.getEntities(((CraftEntity) except).getHandle(), new AxisAlignedBB(xmin, 0, zmin, xmax, 255, zmax))) {
            if (nmsEntity instanceof net.minecraft.server.v1_13_R2.EntityPlayer) {
                players.add((Player) nmsEntity.getBukkitEntity());
            }
        }
        return players;
    }
    
    public void sendMessage(String msg, Entity except) {
        for (Player p : getPlayersInRegion(except)) {
            p.sendMessage(msg);
        }
        if (except != null)
            except.sendMessage(msg);
    }
    
    public void clearChunks() {
        for (Iterator<PredicateChunk> it = containers.iterator(); it.hasNext();) {
            PredicateChunk pc = it.next();
            pc.unregisterRegion(this);
        }
        containers.clear();
    }
    
    public void markChunks() {
        WorldChunkWatcher w = ProtectionHandler.get(this.world, true);
        
        int x = xmin;
        boolean break1 = false;
        Set<Chunk> done = new HashSet<Chunk>();
        while (true) {
            if (x > xmax) {
                x = xmax;
                break1 = true;
            }
            
            int z = zmin;
            boolean break2 = false;
            while (true) {
                if (z > zmax) {
                    z = zmax;
                    break2 = true;
                }
                
                Chunk chunk = new Location(this.world, x, 0, z).getChunk();
                
                if (done.contains(chunk)) {
                    break;
                }
                
                PredicateChunk predicate = w.getPredicate(chunk);
                if (predicate == null) {
                    predicate = w.addPredicate(chunk);
                }
                
                predicate.registerRegion(this);
                done.add(chunk);
                z += 16;
                
                if (break2)
                    break;
            }
            x += 16;
            
            if (break1)
                break;
        }
    }
    
    public void updateArea(PredicateClaim claim) {
        Location loc1 = claim.getFirstPoint();
        Location loc2 = claim.getSecondPoint();

        this.xmin = Math.min(loc1.getBlockX(), loc2.getBlockX());
        this.xmax = Math.max(loc1.getBlockX(), loc2.getBlockX());

        this.zmin = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        this.zmax = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
        
        clearChunks();
        markChunks();
    }
    
    public void openSettings(final Player p) {
        final Inventory settings = Bukkit.createInventory(null, 27, "Claim Settings");
        ItemStack name = NAME.clone();
        Utils.setLore(name, 0, "\"" + this.getName().replace('§', '&') + "\"");
        settings.setItem(11, name);
        
        ItemStack protect = PROTECT.clone();
        Utils.setLore(protect, 1, "§fStatus: " + (isProtected ? "§aProtected!" : "§eUnprotected!"));
        settings.setItem(12, protect);
        
        ItemStack pvp = PVP.clone();
        Utils.setLore(pvp, 0, "§fStatus: " + (isPvp ? "§c☠ ENABLED! ☠" : "§aDisabled!"));
        settings.setItem(14, pvp);
        
        settings.setItem(15, MEMBERS);
        p.openInventory(settings);
        
        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void inv(InventoryClickEvent e) {
                if (e.getInventory().equals(settings)) {
                    e.setCancelled(true);
                    
                    if (e.getClickedInventory() == null || e.getClickedInventory() != e.getView().getTopInventory())
                        return;
                    
                    switch (e.getRawSlot()) {
                        case 11: // name
                            p.closeInventory();
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    changeName(p);
                                }
                            }.runTaskLater(CheezProtect.instance, 1L);
                            break;
                        case 12: // protection
                            isProtected = !isProtected;
                            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, isProtected ? 1.4F : 0.6F);
                            
                            ItemStack protect = PROTECT.clone();
                            Utils.setLore(protect, 1, "§fStatus: " + (isProtected ? "§aProtected!" : "§eUnprotected!"));
                            
                            sendMessage(Main.INFO + "§e" + getName() + " §fis now " + (isProtected ? "§aProtected!" : "§cUnprotected!"), p);
                            settings.setItem(12, protect);
                            break;
                        case 14: // pvp
                            boolean pvp = togglePVP();
                            p.playSound(p.getLocation(), pvp ? Sound.ENTITY_ENDER_DRAGON_GROWL : Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, pvp ? 1.4F : 1.2F);
                            
                            ItemStack pvpStack = PVP.clone();
                            Utils.setLore(pvpStack, 0, "§fStatus: " + (isPvp ? "§c☠ ENABLED! ☠" : "§aDisabled!"));
                            sendMessage(Main.INFO + "PVP in §e" + getName() + " §fis now " + (isPvp ? "§cENABLED!" : "§aDisabled!"), p);
                            settings.setItem(14, pvpStack);
                            break;
                        case 15: // members
                            p.closeInventory();
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    viewMembers(p);
                                }
                            }.runTaskLater(CheezProtect.instance, 1L);
                            break;
                    }
                }
            }
            
            @EventHandler
            public void close(InventoryCloseEvent e) {
                if (e.getInventory().equals(settings)) {
                    unregister();
                }
            }
            
            void unregister() {
                HandlerList.unregisterAll(this);
            }
        }, CheezProtect.instance);
    }
    
    public void viewMembers(final Player p) {
        final Inventory inv = Bukkit.createInventory(null, 54, "Member List");
        LinkedList<String> lore = new LinkedList<String>();
        lore.add("§c[Right Click to Kick from claim]");
        
        for (int i = 0; i < members.size(); i++) {
            UUID id = members.get(i);
            OfflinePlayer op = Bukkit.getOfflinePlayer(id);
            ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
            SkullMeta meta = (SkullMeta) head.getItemMeta();
            meta.setOwner(op.getName());
            meta.setDisplayName("§b" + Main.getName(id));
            lore.addFirst("§9Username: §7" + op.getName());
            meta.setLore(lore);
            lore.removeFirst();
            head.setItemMeta(meta);
            inv.setItem(i, head);
        }
        p.openInventory(inv);

        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void inv(InventoryClickEvent e) {
                if (e.getInventory().equals(inv)) {
                    e.setCancelled(true);
                    if (e.getClickedInventory() == null || e.getClickedInventory() != e.getView().getTopInventory() || e.getCurrentItem() == null)
                        return;
                    
                    ItemStack item = e.getCurrentItem();
                    
                    if (item.getType() == Material.AIR) {
                        unregister();
                        return;
                    }
                    
                    ItemMeta meta = item.getItemMeta();
                    List<String> lore = meta.getLore();
                    
                    String name = ChatColor.stripColor(lore.get(0).split(" ")[1]);
                    OfflinePlayer op = Bukkit.getOfflinePlayer(name);
                    if (op != null) {
                        removeMember(op);
                    }
                    p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 0.6F, 1.0F);
                    inv.setItem(e.getRawSlot(), null);
                }
            }
            
            @EventHandler
            public void close(InventoryCloseEvent e) {
                if (e.getInventory().equals(inv)) {
                    unregister();
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            openSettings(p);
                        }
                    }.runTaskLater(CheezProtect.instance, 1L);
                }
            }
            
            void unregister() {
                HandlerList.unregisterAll(this);
            }
        }, CheezProtect.instance);
    }
    
    public void changeName(final Player p) {
        AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
            @Override
            public void onAnvilClick(AnvilGUI.AnvilClickEvent e) {
                if (e.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                    if (!e.getName().isEmpty()) {
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 1.4F);
                        name = ChatColor.translateAlternateColorCodes('&', e.getName());
                        sendMessage(Main.INFO + "Region name has been changed to: §e" + name + "§f!", p);
                        e.setWillClose(true);
                        e.setWillDestroy(true);
                        
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                openSettings(p);
                            }
                        }.runTaskLater(CheezProtect.instance, 1L);
                        
                        return;
                    }
                    p.sendMessage(Main.ERR + "§cName cannot be empty!");
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.2F);
                }
                e.setWillClose(false);
                e.setWillDestroy(false);
            }
        });
        gui.setSlot(AnvilSlot.INPUT_LEFT, Utils.setName(new ItemStack(Material.PAPER), this.name.replace('§', '&')));
        gui.openInv();
    }
    
    @Deprecated
    public boolean can(Player p, ProtectionParameter parameter) {
        return predicate.can(parameter, getRole(p));
    }
    
    public boolean can(OfflinePlayer p) {
        return !isProtected || p.isOp() || isMember(p);
    }
   
    public int getRole(Player p) {
        return getRole(p.getUniqueId());
    }
    
    public int getRole(UUID id) {
        return roles.get(id);
    }
    
    public List<UUID> getMembers() {
        return members;
    }
    
    public List<UUID> getMods() {
        return mods;
    }
    
    public boolean addMember(OfflinePlayer p) {
        return addMember(p.getUniqueId());
    }
    
    public boolean addMember(UUID id) {
        if (roles.containsKey(id))
            return false;

        Player p = Bukkit.getPlayer(id);
        if (p != null)
            p.sendMessage(Main.INFO + "§fYou've been added to §e" + getName() + "§f!");
        sendMessage(Main.INFO + "§e" + Main.getName(id) + " §fhas been added to §e" + getName() + "§f!", Bukkit.getPlayer(owner));
        roles.put(id, 1);
        members.add(id);
        return true;
    }
    
    public boolean removeMember(OfflinePlayer p) {
        return removeMember(p.getUniqueId());
    }
    
    public boolean removeMember(UUID id) {
        if (roles.remove(id) != null) {
            members.remove(id);

            Player p = Bukkit.getPlayer(id);
            if (p != null)
                p.sendMessage(Main.INFO + "§cYou've been kicked from §e" + getName() + "§c.");
            sendMessage(Main.INFO + "§e" + Main.getName(id) + " §chas been kicked from §e" + getName() + "§c!", Bukkit.getPlayer(owner));
            return true;
        }
        return false;
    }
    
    public String getName() {
        return name;
    }
    
    public String getCoordString() {
        return "§fX:§e" + xmin + " §fZ:§e" + zmin + " §7to " + "§fX:§e" + xmax + " §fZ:§e" + zmax;
    }
    
    @Override
    public void save(ConfigurationSection config) {
        super.save(config);
        config.set("owner", owner.toString());
        
        List<String> roster = new LinkedList<>();
        for (UUID id : mods)
            roster.add(Bukkit.getOfflinePlayer(id).getName() + "," + id.toString() + "," + "2");
        for (UUID id : members)
            roster.add(Bukkit.getOfflinePlayer(id).getName() + "," + id.toString() + "," + "1");
        config.set("members", roster);
        config.set("name", name);
        config.set("protected", isProtected);
        config.set("pvp", isPvp);
        predicate.save(config.isConfigurationSection("predicate") ? config.getConfigurationSection("predicate") : config.createSection("predicate"));
    }

}
