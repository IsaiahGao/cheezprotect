package cheezprotect.util;

import org.bukkit.Chunk;
import org.bukkit.World;

public class ChunkLocation {
    
    public ChunkLocation(Chunk c) {
        this.world = c.getWorld();
        this.x = c.getX();
        this.z = c.getZ();
    }
    
    public ChunkLocation(World world, int x, int z) {
        this.world = world;
        this.x = x + 1;
        this.z = z + 1;
    }
    
    private World world;
    private int x, z;
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof ChunkLocation) {
            ChunkLocation c = (ChunkLocation) o;
            return c.world.getUID().equals(this.world.getUID()) && this.x == c.x && this.z == c.z;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = hash * 19 + this.world.hashCode();
        hash = 31 * hash + this.x;
        hash = 31 * hash + this.z;
        return hash;
    }
    
    public Chunk get() {
        return world.getChunkAt(x, z);
    }
    
    @Override
    public String toString() {
        return this.world.getName() + "," + this.x + "," + this.z;
    }

}
