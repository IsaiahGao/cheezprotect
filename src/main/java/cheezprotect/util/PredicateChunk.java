package cheezprotect.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import cheezprotect.ProtectionHandler;

public class PredicateChunk {
    
    public PredicateChunk(Chunk c) {
        this.world = c.getWorld();
        this.owners = new HashMap<>();
        this.contained = new HashSet<>();
        
        this.xmin = c.getX() * 16;
        this.xmax = this.xmin + 16;
        this.zmin = c.getZ() * 16;
        this.zmax = this.zmin + 16;
    }
    
    public PredicateChunk(ConfigurationSection config) {
        this.owners = new HashMap<>();
        this.contained = new HashSet<>();
        
        String[] pos = config.getString("pos").split(",");
        this.world = Bukkit.getWorld(pos[0]);
        int chunkX = Integer.parseInt(pos[1]);
        int chunkZ = Integer.parseInt(pos[2]);
        this.xmin = chunkX * 16;
        this.xmax = this.xmin + 16;
        this.zmin = chunkZ * 16;
        this.zmax = this.zmin + 16;
        
        for (String item : config.getStringList("regions")) {
            ProtectedRegion2D region = ProtectionHandler.regionCache.get(item);
            if (region == null) {
                System.out.println(item + " was null!! ======================================");
            } else {
                registerRegion(region);
                if (!region.initialized) {
                    ProtectionHandler.registerProtection(region);
                    region.initialized = true;
                }
            }
        }
    }
    
    private World world;
    private int xmin, xmax, zmin, zmax;
    private Map<UUID, Set<ProtectedRegion2D>> owners;

    private Set<ProtectedRegion2D> contained; 
    
    public Chunk getChunk() {
        return new Location(this.world, this.xmin + 1, 0, this.zmin + 1).getChunk();
    }
    
    public World getWorld() {
        return world;
    }
    
    public Set<ProtectedRegion2D> getRegions() {
        return contained;
    }
    
    public void registerRegion(ProtectedRegion2D region) {
        contained.add(region);
        region.addContainer(this);
        
        Set<ProtectedRegion2D> set = owners.get(region.getOwnerUUID());
        if (set == null)
            owners.put(region.getOwnerUUID(), set = new HashSet<ProtectedRegion2D>());
        set.add(region);
    }
    
    public void unregisterRegion(ProtectedRegion2D region) {
        contained.remove(region);
        
        //remove from map too
        Set<ProtectedRegion2D> set = owners.get(region.getOwnerUUID());
        if (set != null) {
            if (set.size() <= 1) {
                owners.remove(region.getOwnerUUID());
            } else {
                set.remove(region);
            }
        }
        
        if (contained.isEmpty())
            ProtectionHandler.get(world, false).removePredicate(getChunk());
    }
    
    public ProtectedRegion2D get(Location loc) {
        for (ProtectedRegion2D region : contained) {
            if (region.within(loc))
                return region;
        }
        return null;
    }
    
    public boolean canInteract(Player p, Location loc) {
        Set<ProtectedRegion2D> regions = owners.get(p.getUniqueId());
        if (regions != null) {
            for (ProtectedRegion2D region : regions)
                if (region.within(loc))
                    return true;
        }
        return false;
    }
    
    public void save(ConfigurationSection config) {
        config.set("pos", world.getName() + "," + (xmin / 16) + "," + (zmin / 16));
        Set<String> regionList = new HashSet<>();
        for (Set<ProtectedRegion2D> regions : owners.values())
            for (ProtectedRegion2D region : regions)
                regionList.add(region.getId());
        config.set("regions", new ArrayList<>(regionList));
    }

}
