package cheezprotect.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import cheezprotect.ProtectionHandler;

public class WorldChunkWatcher {
    
    public WorldChunkWatcher(World world) {
        this.world = world;
    }
    
    public WorldChunkWatcher(ConfigurationSection config) {
        this.world = Bukkit.getWorld(config.getName());
        ProtectionHandler.watchedWorlds.put(world, this);
        
        ConfigurationSection chunks = config.getConfigurationSection("chunks");
        for (String s : chunks.getKeys(false)) {
            PredicateChunk chunk = new PredicateChunk(chunks.getConfigurationSection(s));
            watchedChunks.put(new ChunkLocation(chunk.getChunk()), chunk);
        }
    }
    
    private World world;
    private Map<ChunkLocation, PredicateChunk> watchedChunks = new HashMap<>();
    private Map<UUID, HashSet<ProtectedRegion2D>> claimedRegions = new HashMap<>();
    public int totalRegions = 0;
    
    public World getWorld() {
        return world;
    }
    
    public Map<ChunkLocation, PredicateChunk> getChunkMap() {
        return watchedChunks;
    }
    
    public Map<UUID, HashSet<ProtectedRegion2D>> getAllRegions() {
        return claimedRegions;
    }
    
    public HashSet<ProtectedRegion2D> getPlayerRegions(UUID id) {
        return claimedRegions.get(id);
    }
    
    public PredicateChunk getPredicate(Chunk chunk) {
        ChunkLocation c = new ChunkLocation(chunk);
        return watchedChunks.get(c);
    }
    
    public PredicateChunk getPredicate(World world, int x, int z) {
        ChunkLocation c = new ChunkLocation(world, x, z);
        return watchedChunks.get(c);
    }
    
    public PredicateChunk addPredicate(Chunk chunk) {
        ChunkLocation c = new ChunkLocation(chunk);
        PredicateChunk predicate = watchedChunks.get(c);
        if (predicate != null) {
            return predicate;
        }
        predicate = new PredicateChunk(chunk);
        watchedChunks.put(c, predicate);
        return predicate;
    }
    
    public PredicateChunk removePredicate(Chunk chunk) {
        ChunkLocation c = new ChunkLocation(chunk);
        PredicateChunk pc = watchedChunks.remove(c);
        return pc;
    }
    
    public ProtectedRegion2D registerProtection(ProtectedRegion2D region) {
        HashSet<ProtectedRegion2D> set = claimedRegions.get(region.getOwnerUUID());
        if (set == null)
            claimedRegions.put(region.getOwnerUUID(), set = new HashSet<ProtectedRegion2D>());
        if (set.add(region))
            this.totalRegions++;
        return region;
    }
    
    public ProtectedRegion2D removeProtection(ProtectedRegion2D region) {
        HashSet<ProtectedRegion2D> set = claimedRegions.get(region.getOwnerUUID());
        if (set == null)
            return null;
        if (set.remove(region)) {
            System.out.println("found hashing: " + region.hashCode());
            this.totalRegions--;
        } else {
            System.out.println("no hash found: " + region.hashCode());
        }
        region.clearChunks();
        return region;
    }
    
    public HashSet<ProtectedRegion2D> removeProtection(Player p) {
        return claimedRegions.remove(p.getUniqueId());
    }
    
    public HashSet<ProtectedRegion2D> getClaimedRegions(Player p) {
        return getClaimedRegions(p.getUniqueId());
    }
    
    public HashSet<ProtectedRegion2D> getClaimedRegions(UUID id) {
        return claimedRegions.getOrDefault(id, new HashSet<ProtectedRegion2D>());
    }

    public boolean canInteract(Player p, Location loc) {
        PredicateChunk predicate = watchedChunks.get(new ChunkLocation(loc.getChunk()));
        if (predicate != null) {
            return predicate.canInteract(p, loc);
        }
        return false;
    }

    public void save(ConfigurationSection config) {
        ConfigurationSection chunks = config.getConfigurationSection("chunks");
        if (chunks == null)
            chunks = config.createSection("chunks");
        
        int j = 0;
        for (PredicateChunk predicate : watchedChunks.values()) {
            predicate.save(chunks.createSection("" + j));
            j++;
        }
    }
    
}
