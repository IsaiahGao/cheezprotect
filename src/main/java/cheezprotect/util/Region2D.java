package cheezprotect.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

import cheezprotect.exceptions.InvalidRegionException;

public class Region2D {
    
    public Region2D(Location loc1, Location loc2) {
        this.world = loc1.getWorld();
        
        if (this.world != loc2.getWorld() || loc1.equals(loc2))
            throw new InvalidRegionException();
        
        this.xmin = Math.min(loc1.getBlockX(), loc2.getBlockX());
        this.xmax = Math.max(loc1.getBlockX(), loc2.getBlockX());

        this.zmin = Math.min(loc1.getBlockZ(), loc2.getBlockZ());
        this.zmax = Math.max(loc1.getBlockZ(), loc2.getBlockZ());
    }
    
    public Region2D(ConfigurationSection config) {
        this.world = Bukkit.getWorld(config.getString("world"));
        String[] x = config.getString("range-x").split(",");
        this.xmin = Integer.parseInt(x[0]);
        this.xmax = Integer.parseInt(x[1]);
        
        String[] z = config.getString("range-z").split(",");
        this.zmin = Integer.parseInt(z[0]);
        this.zmax = Integer.parseInt(z[1]);
    }
    
    public World world;
    public int xmin, zmin, xmax, zmax;
    
    public World getWorld() {
        return world;
    }
    
    public Location minPoint() {
        return new Location(world, xmin, 0, zmin);
    }
    
    public Location maxPoint() {
        return new Location(world, xmax, 256, zmax);
    }
    
    public boolean within(Location loc) {
        return this.within(loc.getBlockX(), loc.getBlockZ());
    }
    
    public boolean within(int x, int z) {
        return this.xmin <= x
                && x <= this.xmax
                && this.zmin <= z
                && z <= this.zmax;
    }
    
    public int area() {
        return (xmax - xmin + 1) * (zmax - zmin + 1);
    }
    
    public void save(ConfigurationSection config) {
        config.set("world", world.getName());
        config.set("range-x", this.xmin + "," + this.xmax);
        config.set("range-z", this.zmin + "," + this.zmax);
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Region2D) {
            Region2D r = (Region2D) o;
            return this.world == r.world
                    && r.xmax == this.xmax
                    && r.xmin == this.xmin
                    && r.zmin == this.zmin
                    && r.zmax == this.zmax;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        int hash = this.world.hashCode();
        hash += 19 * (this.xmin + 3);
        hash += 19 * (this.xmax + 3);
        hash += 19 * (this.zmin + 4);
        hash += 19 * (this.zmax + 4);
        return hash;
    }

}
