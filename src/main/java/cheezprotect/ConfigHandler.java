package cheezprotect;

import java.io.File;
import java.io.IOException;

public class ConfigHandler {
    
    public static void save() {
        try {
            CheezProtect.config.save("plugins" + File.separator + "CheezProtect" + File.separator + "config.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void reload() {
        CheezProtect.instance.reloadConfig();
        CheezProtect.config = CheezProtect.instance.getConfig();
    }

}
