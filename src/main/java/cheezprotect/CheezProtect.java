package cheezprotect;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.plugin.java.JavaPlugin;

import cheezprotect.claiming.ClaimHandler;
import cheezprotect.listeners.ProtectionListener;
import cheezprotect.listeners.RegionListener;
import cheezprotect.util.ChunkLocation;
import cheezprotect.util.ProtectedRegion2D;
import cheezprotect.util.WorldChunkWatcher;
import cwhitelist.Main;
import utilities.Utils;

public class CheezProtect extends JavaPlugin {
    
    public static FileConfiguration config;
    public static CheezProtect instance;
    
    public static final String[] HELP_TEXT = {
            "§m------------------§r[§eClaim Commands§f]§m------------------",
            "§fSelect a region by Right Clicking two corners with a Feather!",
            "§6/claim view §7View all your claims, their IDs, and your remaining claim quota.",
            "§6/claim confirm §7Confirm a claim after selecting a region.",
            "§6/claim cancel §7Cancel a region selection.",
            "§6/claim add [Player] [ID?] §7Add a player to the member list of a claim you own with specified ID, or the current claim you're standing in if none of provided.",
            "§6/claim remove [Player] [ID?] §7Remove a player from the member list of a claim you own with specified ID, or the current claim you're standing in if none of provided.",
            "§6/claim info [ID?] §7View claim info for a certain ID, or the current claim you're standing in if none is provided.",
            "§6/claim settings [ID?] §7Open settings for a certain ID, or the current claim you're standing in if none is provided.",
            "§6/claim unclaim [ID?] §7Unclaim a claim with a certain ID, or the current claim you're standing in if none is provided. Will ask for confirmation.",
    };
    
    public static final String[] ADMIN_HELP_TEXT = {
            "§c/claim list §7Lists all claims, their owners, and their IDs.",
            "§c/claim forceadd [ID?] [Player] §7Forcefully adds a player to a claim.",
            "§c/claim forceremove [ID?] [Player] §7Forcefully removes a player from a claim.",
            "§c/claim tp [ID] §7Teleports to a claim.",
            "§c/claim delete [ID?] §7Forcefully deletes a claim.",
    };
    
    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        
        instance = this;
        config = this.getConfig();

        this.getServer().getPluginManager().registerEvents(new ProtectionListener(), this);
        this.getServer().getPluginManager().registerEvents(new RegionListener(), this);
        new ClaimHandler(this);
        ProtectionHandler.load(config);
    }
    
    @Override
    public void onDisable() {
        ProtectionHandler.save(config);
        
        try {
            config.save("plugins" + File.separator + "CheezProtect" + File.separator + "config.yml");
        } catch (Exception e) {
            
        }
    }
    
    @Override
    public boolean onCommand(CommandSender sender, Command lbl, String cmd, String[] args) {
        if (cmd.equalsIgnoreCase("claim")) {
            if (!(sender instanceof Player))
                return true;
            
            Player p = (Player) sender;
            if (args.length > 0) {
                switch (args[0].toLowerCase()) {
                    case "view":
                        p.sendMessage("§cYour Claimed Territory:");
                        boolean flag = false;
                        for (ProtectedRegion2D region : ProtectionHandler.getRegions(p.getUniqueId())) {
                            p.sendMessage("§e§ ID: §f" + region.getId() + " §e| Name: f" + region.getName());
                            p.sendMessage("  §7World: §f" + Utils.capitalizeFirst(region.getWorld().getName()));
                            p.sendMessage("  §7Size: §f" + region.area() + "§7 blocks");
                            p.sendMessage("  §7Coords: §f" + region.getCoordString());
                            flag = true;
                        }
                        if (!flag) {
                            p.sendMessage("§7  § None");
                        }
                        p.sendMessage("§cYou can claim another §e" + (ProtectionHandler.getMaxClaim(p.getWorld()) - ProtectionHandler.getClaimedArea(p, p.getWorld())) + "§c/" + ProtectionHandler.getMaxClaim(p.getWorld()) + " blocks.");
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        return true;
                    case "confirm":
                        ClaimHandler.confirmClaim(p);
                        return true;
                    case "cancel":
                        ClaimHandler.cancelClaim(p);
                        return true;
                    case "add":
                        ProtectionHandler.addOrRemove(true, p, args, false);
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        return true;
                    case "remove":
                        ProtectionHandler.addOrRemove(false, p, args, false);
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        return true;
                    case "info":
                        ProtectedRegion2D region = ProtectionHandler.getRegion(p, args, 1);
                        if (region == null) {
                            p.sendMessage(Main.ERR + (args.length > 1 ? "§cNo such region with ID: §e" + args[1] : "§cYou aren't standing in any claimed regions!"));
                            return true;
                        }
                        p.sendMessage("§6[REGION] ID: §e" + region.getId() + " §6| Name: §e" + region.getName());
                        
                        boolean owner = region.getOwnerUUID().equals(p.getUniqueId());
                        p.sendMessage("§cOwner: §f" + (owner ? "You" : Main.getName(region.getOwnerUUID())));
                        p.sendMessage("§cMembers: §f" + region.getMembers().size());
                        if (owner || p.isOp() || region.isMember(p))
                            for (UUID id : region.getMembers())
                                p.sendMessage((Bukkit.getPlayer(id) == null ? "  §7§ " : "  §a§ ") + Main.getName(id));
                        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        return true;
                    case "settings":
                    case "edit":
                        region = ProtectionHandler.getRegion(p, args, 1);
                        if (region == null) {
                            p.sendMessage(Main.ERR + (args.length > 1 ? "§cNo such region with ID: §e" + args[1] : "§cYou aren't standing in any claimed regions!"));
                            return true;
                        }
                        
                        if (!region.getOwnerUUID().equals(p.getUniqueId())) {
                            p.sendMessage(Main.ERR + "§cYou cannot change settings of regions you do not own.");
                            return true;
                        }
                        
                        region.openSettings(p);
                        return true;
                    case "unclaim":
                        ProtectionHandler.deleteRegion(p, args, false);
                        return true;
                        
                    // admin commands
                    case "list":
                        if (p.isOp()) {
                            p.sendMessage("§6All claimed regions in current world:");
                            WorldChunkWatcher wcw = ProtectionHandler.get(p.getWorld(), false);
                            if (wcw == null) {
                                p.sendMessage("§7§ §fNone");
                            } else {
                                for (HashSet<ProtectedRegion2D> set : wcw.getAllRegions().values()) {
                                    for (ProtectedRegion2D r : set) {
                                        p.sendMessage("§7§ §f" + r.getId() + ": §e" + r.getName() + ": §6Owner: " + r.getOwner().getName());
                                    }
                                }   
                            }
                            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        }
                        return true;
                    case "forceadd":
                        if (p.isOp()) {
                            ProtectionHandler.addOrRemove(true, p, args, true);
                            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        }
                        return true;
                    case "forceremove":
                        if (p.isOp()) {
                            ProtectionHandler.addOrRemove(false, p, args, true);
                            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.8F, 1.4F);
                        }
                        return true;
                    case "tp":
                        if (p.isOp()) {
                            if (args.length < 2) {
                                p.sendMessage(Main.ERR + "§cNo region specified.");
                                return true;
                            }
                            
                            region = ProtectionHandler.getRegion(p, args, 1);
                            if (region == null) {
                                p.sendMessage(Main.ERR + "§cNo such region with ID: §e" + args[1]);
                                return true;
                            }
                            
                            Location loc = new Location(region.world, region.xmin, 0, region.zmin);
                            loc.setY(loc.getWorld().getHighestBlockYAt(loc));
                            p.teleport(loc);
                            p.sendMessage(Main.INFO + "§7Teleported to region with ID: §e" + args[1]);
                        }
                        return true;
                    case "delete":
                        if (p.isOp()) {
                            ProtectionHandler.deleteRegion(p, args, true);
                        }
                        return true;
                    case "test":
                        if (p.isOp()) {
                            Map<ChunkLocation, String> map = new HashMap<>();
                            map.put(new ChunkLocation(p.getLocation().getChunk()), "a");
                            Chunk c = p.getLocation().getChunk();
                            Location loc = p.getLocation();
                            p.sendMessage("Chunk: " + c.getWorld().getName() + " " + c.getX() + " " + c.getZ());
                            p.sendMessage("Coord: " + loc.getWorld().getName() + " " + loc.getBlockX() / 16 + " " + loc.getBlockZ() / 16);
                            p.sendMessage("Chunk Result: " + map.get(new ChunkLocation(p.getLocation().getChunk())));
                            p.sendMessage("Coord Result: " + map.get(new ChunkLocation(p.getWorld(), p.getLocation().getBlockX() / 16, p.getLocation().getBlockZ() / 16)));
                        }
                        return true;
                }
            }
            
            for (String s : HELP_TEXT) {
                p.sendMessage(s);
            }
            if (p.isOp()) {
                for (String s : ADMIN_HELP_TEXT)
                    p.sendMessage(s);
            }
            return true;
        }
        return false;
    }
    
    public static boolean playerCanAttack(Player attacker, LivingEntity defender) {
    	boolean player = defender instanceof Player || (defender instanceof Tameable && ((Tameable) defender).isTamed());
    	if (player && (defender.getLocation().getWorld() == Utils.misc || defender.getLocation().getWorld() == Utils.shadowrealm)) {
    		return false;
    	}
    	
    	ProtectedRegion2D region = ProtectionHandler.getRegion(defender.getLocation());
    	if (region != null) {
    		return player ? region.isPVP() : !ProtectionListener.isProtected(defender) || region.can(attacker);
    	}
    	return true;
    }

}
